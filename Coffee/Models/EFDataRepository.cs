﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coffee.Models
{
    public class EFDataRepository : IDataRepository
    {
        private ApplicationDbContext context;
        public EFDataRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }
        public IQueryable<Comment> Comments => context.Comments;
        public IQueryable<Order> Orders => context.Orders;

        public void SaveOrder (Order order)
        {
            if(order.OrderId == 0)
            {
                context.Orders.Add(order);
            }
            context.SaveChanges();
        }

        public void SaveComment(Comment comment)
        {
            if(comment.CommentId == 0)
            {
                context.Comments.Add(comment);
            }
            context.SaveChanges();
        }
    }
}
