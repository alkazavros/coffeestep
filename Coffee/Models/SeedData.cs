﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coffee.Models
{
    public static class SeedData
    {
        public static void EnsurePopulated(IApplicationBuilder app)
        {
            ApplicationDbContext context = app.ApplicationServices
                .GetRequiredService<ApplicationDbContext>();
            context.Database.Migrate();
            if (!context.Comments.Any())
            {
                context.Comments.AddRange(
                        new Comment
                        {
                            Name = "TestName",
                            Phone = 1245678,
                            Text = "Sample text"

                        }
                    );

                context.SaveChanges();
            }
        }
    }
}
