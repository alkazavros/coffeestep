﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Coffee.Models
{
    public class Comment
    {
        [BindNever]
        public int CommentId { get; set; }
        [Required(ErrorMessage = "Пожалуйста, введите имя")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Пожалуйста, введите телефон")]
        public int Phone { get; set; }
        [Required(ErrorMessage = "Пожалуйста, введите комментарий")]
        public string Text { get; set; }
    }
}
