﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coffee.Models
{
    public interface IDataRepository 
    {
        IQueryable<Comment> Comments { get; }
        IQueryable<Order> Orders { get; }
        void SaveOrder(Order order);
        void SaveComment(Comment comment);
    }
}
