﻿using Coffee.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coffee.Controllers
{
    public class CommentController : Controller
    {
        private IDataRepository repository;
        public CommentController(IDataRepository repoService)
        {
            repository = repoService;
        }
        public ViewResult Add() => View(new Comment());
        [HttpPost]
        public IActionResult Add(Comment comment)
        {
            if (ModelState.IsValid)
            {
                repository.SaveComment(comment);
                return RedirectToAction(nameof(Completed));
            }
            else
            {
                return View(comment);
            }
        }
        public ViewResult Completed()
        {
            return View();
        }
    }
}
