﻿using Coffee.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Coffee.Controllers
{
    public class HomeController : Controller
    {
        private IDataRepository repository;
        public HomeController(IDataRepository repoService)
        {
            repository = repoService;
        }
        public ViewResult Index()
        {
            return View(new Order());
        }
        [HttpPost]
        public IActionResult Checkout(Order order)
        {
            if (ModelState.IsValid)
            {
                repository.SaveOrder(order);
                return RedirectToAction(nameof(Completed));
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }
        public ViewResult Completed()
        {
            return View();
        }
        public IActionResult CommentsPartial()
        {
            var listComments = repository.Comments.ToList();
            return PartialView(listComments);
        }
    }
}
